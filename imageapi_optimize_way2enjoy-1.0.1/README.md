INTRODUCTION
------------

The Image Optimize way2enjoy module provides integration with the way2enjoy service
for the Image Optimize pipeline system.

  * For a full description of the module, visit the project page:
    https://drupal.org/project/imageapi_optimize_way2enjoy

  * To submit bug reports and feature suggestions, or to track changes:
    https://drupal.org/project/issues/imageapi_optimize_way2enjoy


REQUIREMENTS
------------

This module requires the following modules:

  * Image Optimize (https://drupal.org/project/imageapi_optimize)


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. Visit:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.


CONFIGURATION
-------------

  * Configure Image Optimize pipelines in Administration » Configuration » Media
    » Image Optimize pipelines:

    * Either add a new pipeline or edit an existing one.

    * Add a new Way2enjoy processor.
